using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;


namespace TestGame1
{
    /// <summary>
    /// This is the main type for your game
    /// </summary>
    public class Game1 : Microsoft.Xna.Framework.Game
    {
        public GraphicsDeviceManager graphics;
        SpriteBatch spriteBatch;

        Functions Func = new Functions();
        Settings Config = new Settings();
        IResistance Resistance = new CResistance();

        public Game1()
        {
            graphics = new GraphicsDeviceManager(this);
            Content.RootDirectory = "Content";
        }

        /// <summary>
        /// Allows the game to perform any initialization it needs to before starting to run.
        /// This is where it can query for any required services and load any non-graphic
        /// related content.  Calling base.Initialize will enumerate through any components
        /// and initialize them as well.
        /// </summary>
        protected override void Initialize()
        {
            // TODO: Add your initialization logic here
            graphics.SynchronizeWithVerticalRetrace = false;
            

            IsFixedTimeStep = false;
            Config.gameDelay = 1;

            //graphics.ApplyChanges();
       
            base.Initialize();

        }

        /// <summary>
        /// LoadContent will be called once per game and is the place to load
        /// all of your content.
        /// </summary>
        /// 
       


        Texture2D PlayerSprite;
        SpriteFont font;
        Vector2 PlayerPos = Vector2.Zero;
        Vector2 PlayerVel = new Vector2(0, 0);
        /*Vector2 PlayerOrigin;
        float PlayerRot = 0f;
        double frameRate = 0.0;*/

        int _total_frames = 0;
        float _elapsed_time = 0.0f;
        int _fps = 30;
        private Rectangle TitleSafe;
        const float ROTATIONAMOUNT = 0.1f;
        float rRotationAmount;
        const float tangentialAcceleration = 5f;
        float rAcceleration;
        const float FRICTION = 0.05f;
        float rFriction;
        const float MAXVELOCITY = 30f;
        int maxX;
        int maxY;

       
        
        Texture2D Planet;
        static Vector2 PlanetPos = new Vector2(500, 300);
        Vector2 PlanetOrigin;

        Entity Player = new Entity(new Vector2(100, 100), new Vector2(0,0), 0f);


        
       


        protected override void LoadContent()
        {
            // Create a new SpriteBatch, which can be used to draw textures.
            spriteBatch = new SpriteBatch(GraphicsDevice);


            PlayerSprite = Content.Load<Texture2D>("RedShip");
            Player.Sprite = PlayerSprite;

           // Entity Player = new Entity(PlayerSprite, PlayerPos, PlayerVel, PlayerOrigin, PlayerRect, PlayerRot);

            Planet = Content.Load<Texture2D>("Nought");

            font = Content.Load<SpriteFont>("myFont");

            Viewport viewport = graphics.GraphicsDevice.Viewport;
            // TODO: use this.Content to load your game content here
        }

        /// <summary>
        /// UnloadContent will be called once per game and is the place to unload
        /// all content.
        /// </summary>
        protected override void UnloadContent()
        {
            // TODO: Unload any non ContentManager content here
        }

        /// <summary>
        /// Allows the game to run logic such as updating the world,
        /// checking for collisions, gathering input, and playing audio.
        /// </summary>
        /// <param name="gameTime">Provides a snapshot of timing values.</param>
        /// 

        

        protected override void Update(GameTime gameTime)
        {
            Texture2D PlayerSprite = Player.Sprite;
            Vector2 PlayerVel = Player.Vel;
            Vector2 PlayerPos = Player.Pos;
            

           _elapsed_time += (float)gameTime.ElapsedGameTime.TotalMilliseconds;
 
            // 1 Second has passed
            if (_elapsed_time >= 1000.0f)
            {
                _fps = _total_frames;
                _total_frames = 0;
                _elapsed_time = 0;
                Config.gameDelay = (_fps / 60);
            }


            rAcceleration = tangentialAcceleration;// / Config.gameDelay;
            rFriction = FRICTION;// / Config.gameDelay;
            rRotationAmount = ROTATIONAMOUNT;//Func.double2Float(((double)(ROTATIONAMOUNT / Config.gameDelay)));

            // Allows the game to exit
            if (Keyboard.GetState().IsKeyDown(Keys.Escape))
                this.Exit();
            //Entity Player = new Entity(PlayerSprite, PlayerPos, PlayerVel, PlayerOrigin, PlayerRect, PlayerRot);

            //Gets delta time
            double Delta = (float)gameTime.ElapsedGameTime.TotalSeconds; 

    

            PlayerPos += PlayerVel * (float)Delta;
            Player.Origin = new Vector2(PlayerSprite.Width / 2, PlayerSprite.Height / 2);
          

            PlanetOrigin = new Vector2(Planet.Width / 2, Planet.Height / 2);

            maxX = GraphicsDevice.Viewport.Width - Player.Sprite.Width;
            maxY = GraphicsDevice.Viewport.Height - Player.Sprite.Height;

            if (Keyboard.GetState().IsKeyDown(Keys.Right))
                Player.Rot += rRotationAmount;
            if (Keyboard.GetState().IsKeyDown(Keys.Left))
                Player.Rot -= rRotationAmount;
            if (Keyboard.GetState().IsKeyDown(Keys.Enter))
                ;
            if (Keyboard.GetState().IsKeyDown(Keys.Z))
                Resistance.Toggle();
            if (Keyboard.GetState().IsKeyDown(Keys.Up))
            {

                
                PlayerVel.X += ((float)Math.Cos(Player.Rot) * rAcceleration);
                PlayerVel.Y += ((float)Math.Sin(Player.Rot) * rAcceleration);
                /*if (PlayerVel.X > MAXVELOCITY)  PlayerVel.X = MAXVELOCITY; 
                if (PlayerVel.X < -MAXVELOCITY) PlayerVel.X = -MAXVELOCITY;
                if (PlayerVel.Y > MAXVELOCITY)  PlayerVel.Y= MAXVELOCITY;
                if (PlayerVel.Y < -MAXVELOCITY) PlayerVel.Y = -MAXVELOCITY;*/
            }
            else if (PlayerVel != Vector2.Zero)
            {
               PlayerVel.X -= rFriction * PlayerVel.X;
               PlayerVel.Y -= rFriction * PlayerVel.Y;
            }

            if (PlayerVel != Vector2.Zero && PlayerVel.X < 0.01f)
            {
                PlayerVel.X = 0f;
            }
            if (PlayerVel != Vector2.Zero && PlayerVel.Y < 0.01f)
            {
                PlayerVel.Y = 0f;
            }

            if (PlayerPos.X > maxX || PlayerPos.X < 0)
            {
                PlayerVel.X *= -1;
                if (PlayerPos.X > maxX)
                {
                    PlayerPos.X = maxX - 1;
                }
                else
                {
                    PlayerPos.X = 1;
                }


            }
            if (PlayerPos.Y > maxY || PlayerPos.Y < 0)
            {
                PlayerVel.Y *= -1;
                if (PlayerPos.Y > maxY)
                {
                    PlayerPos.Y = maxY - 1;
                }
                else
                {
                    PlayerPos.Y = 1;
                }

            }



            Player.Vel = PlayerVel;
            Player.Pos = PlayerPos;

            base.Update(gameTime);
        }

        protected Rectangle GetTitleSafeArea(float percent)
        {
            Rectangle retval = new Rectangle(
                graphics.GraphicsDevice.Viewport.X,
                graphics.GraphicsDevice.Viewport.Y,
                graphics.GraphicsDevice.Viewport.Width,
                graphics.GraphicsDevice.Viewport.Height);

            return retval;
        }   

        /// <summary>
        /// This is called when the game should draw itself.
        /// </summary>
        /// <param name="gameTime">Provides a snapshot of timing values.</param>
        protected override void Draw(GameTime gameTime)
        {

            _total_frames++;



            GraphicsDevice.Clear(Color.White);
            spriteBatch.Begin();
            Vector2 pos = new Vector2(TitleSafe.Left, TitleSafe.Top);
            spriteBatch.DrawString(font, string.Format("Speed: X: {0} Y: {1} Pos: {2}/{3},{4}/{5}"  , Player.Vel.X.ToString(),Player.Vel.Y.ToString(),Player.Pos.X.ToString(),maxX,Player.Pos.Y.ToString(),maxY), new Vector2(0, 0), Color.Black);
            spriteBatch.DrawString(font, "resistance: " + Resistance , new Vector2(0, 25), Color.Black);
            spriteBatch.DrawString(font, "FPS: "+ _fps, new Vector2(0,50), Color.Black);
            spriteBatch.DrawString(font, "FPS / 60: " + ((int)_fps / 60), new Vector2(0, 75), Color.Black);
            spriteBatch.Draw(Player.Sprite, Player.Pos, null, Color.White, Player.Rot, Player.Origin, 1f, SpriteEffects.None, 0);
            //RedShipSprite, RedShipPosition, null,Color.White, RedShipRotationAngle, RedShipOrigin, Game1.0f, SpriteEffects.None, 0f);

            spriteBatch.Draw(Planet, PlanetPos, null, Color.White,0, PlanetOrigin, 1f, SpriteEffects.None, 0);

            spriteBatch.End();
            // TODO: Add your drawing code here
            
            base.Draw(gameTime);
        }





    }
    
}
