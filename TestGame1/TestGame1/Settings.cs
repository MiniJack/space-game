﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace TestGame1
{
    public class Settings
    {
        public float gameDelay { get; set; }
    }


    public interface IResistance
    {
        void Toggle();
        public bool _resistance { get; set; }
    }

    public class CResistance : IResistance
    {
        bool _resistance { get; set; }
        public void Toggle()
        {
            if (_resistance)
            {
                _resistance = false;
            }
            else
            {
                _resistance = true;
            }
        }
        public bool get()
        {
            return _resistance;
        }



    }


}
