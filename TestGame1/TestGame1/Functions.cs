﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace TestGame1
{

    public class Functions
    {
        public static double G = Math.Pow(6.673*10,-11);

        //Returns greater value
        public double Max(double a, double b)
        {
            
            if (a > b) { return a; }
            else { return b; }
        }

        //Returns smaller value
        public double Min(double a, double b)
        {
            if (a > b) { return b; }
            else { return a; }
        }

        public double Diff(double a, double b) {
            return Max(a, b) - Min(a, b);
        }

        public double Ball(double Mass1, double Mass2, double Distance) {
            double f = G * ((Mass1 * Mass2) / Math.Pow(Distance,2));
            return f;
        }

        public double force2Acceleration(double Distance, double mass2)
        {
            return G * (mass2 / Math.Pow(Distance, 2));
        }

        public double[] numeric_solve(double TimeInterval,double v,double a)
        {
            double p = 0;
            double t = 0;
            double max = 0;
            while (p >= 0){

                v += a * TimeInterval;
                p += v * TimeInterval;
                t += TimeInterval;
                if (p > max) max = p;
            }
            double[] re = { t, max };
            return re;
            
        }


        private static int lastTick;
        private static int lastFrameRate;
        private static int frameRate;

        public static int CalculateFrameRate()
        {
            if (System.Environment.TickCount - lastTick >= 1000)
            {
                lastFrameRate = frameRate;
                frameRate = 0;
                lastTick = System.Environment.TickCount;
            }
            frameRate++;
            return lastFrameRate;
        }

        public float double2Float(double input)
        {
            float num = (float) input;
            if (float.IsPositiveInfinity(num))
            {
                num = float.MaxValue;
            }
            else if (float.IsNegativeInfinity(num))
            {
                num = float.MinValue;
            }
            return num;
        }

    }
    
}

