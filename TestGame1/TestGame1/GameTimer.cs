﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace TestGame1
{
    public class GameTimer : Microsoft.Xna.Framework.GameComponent
    {
        /// Used to keep track of how much time since the previous Tick.
        protected TimeSpan ElapsedTime { get; set; }

        /// Gets or sets the amount of time between timer ticks.
        public virtual TimeSpan Interval { get; set; }

        /// Gets or sets a user-defined data object.
        public Object Tag { get; set; }

        /// Gets or sets a value that indicates whether the timer is running.  
        public bool IsEnabled { get; set; }

        public GameTimer(Microsoft.Xna.Framework.Game game)
            : base(game)
        {
        }

        public GameTimer(Microsoft.Xna.Framework.Game game, TimeSpan interval)
            : this(game)
        {
            Interval = interval;
        }

        /// Starts the GameTimer.
        public void Start()
        {
            IsEnabled = true;
        }
        /// Stops the GameTimer.
        public void Stop()
        {
            // Disable the timer.
            IsEnabled = false;

            // Reset the elapsed time to 0.
            ElapsedTime = TimeSpan.Zero;
        }

        public override void Update(Microsoft.Xna.Framework.GameTime gameTime)
        {
            bool ticked = false;

            // Figure out how many Ticks we need.
            if (IsEnabled && this.Interval > TimeSpan.Zero)
            {
                var args = new GameTimeEventArgs(gameTime);

                ElapsedTime = ElapsedTime.Add(gameTime.ElapsedGameTime);
                while (IsEnabled && ElapsedTime >= Interval)
                {
                    if (!ticked)
                    {
                        ticked = true;
                        OnTickFirst(args);
                    }

                    ElapsedTime = ElapsedTime.Subtract(Interval);
                    OnTick(args);
                }

                // If we know we ticked at least once during this Update, raise event that we're on the last Tick.
                if (ticked)
                {
                    OnTickLast(args);
                }
            }
            /*else
            {
                ElapsedTime = TimeSpan.Zero;
            }*/

            base.Update(gameTime);
        }

        public event EventHandler<GameTimeEventArgs> TickFirst;

        /// Event fired during Update for each tick defined by the interval set on the timer.  
        /// Depending on how quickly the update loop is running, this event may fire 0-N times during a single Update.
        public event EventHandler<GameTimeEventArgs> Tick;

        public event EventHandler<GameTimeEventArgs> TickLast;

        protected virtual void OnTickFirst(GameTimeEventArgs e)
        {
            if (TickFirst != null)
                TickFirst(this, e);
        }

        protected virtual void OnTick(GameTimeEventArgs e)
        {
            if (Tick != null)
                Tick(this, e);
        }

        protected virtual void OnTickLast(GameTimeEventArgs e)
        {
            if (TickLast != null)
                TickLast(this, e);
        }
    }

    /// Event data containing information about game timing state typically used during Update or Draw methods.
    public class GameTimeEventArgs : EventArgs
    {
        public Microsoft.Xna.Framework.GameTime GameTime { get; set; }

        public GameTimeEventArgs()
            : base()
        {
        }

        public GameTimeEventArgs(Microsoft.Xna.Framework.GameTime gameTime)
            : base()
        {
            GameTime = gameTime;
        }
    }
}
